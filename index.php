<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>ASCII-merkit 32-255</title>
</head>
<body>
<h3>ASCII</h3>
<table>
<?php
header('Content-Type: text/html; charset=ISO-8859-1');
$num = range(32, 255);
$marks = array_map('chr', $num);

    foreach ($marks as $value) {       
        print "$value ";
    }    
          
?>
</table>
</body>
</html>